#!/bin/bash

COOJA_DIR=../tools/cooja;
#This command works in this directory java -jar ../tools/cooja/dist/cooja.jar -nogui=/home/user/contiki-rpl-ee/simulations/hello_prova.csc -contiki=..
COOJA_CMD="java -jar ../tools/cooja/dist/cooja.jar ";
CONTIKI_ROOT=~
CONF_FILE=../examples/contiki-ee/project-conf.h
CONF_TEMPLATE=./objective_functions/conf-template.h
DEFAULT_TOPOLOGY=../examples/contiki-ee/test-sim.csc
TEMPORARY_DIRECTORY=/tmp/cont
COOJA_LOG=COOJA.*
RESULT_DIRECTORY=~/contiki-simulation
REPETITIONS=1
RANDOM_SEED=('12249' '22067' '18382' '21502' '2149' '9286' '3976' '13129' '10890' '23397' '30405' '8805' '13673' '15068' '32128' '25848' '29260' '6610' '19396' '19468')

# DEFAULT 30 MIN
SIMTIME=1800000

# Settare a 4 in caso di quad-core
PARALLELISM_LEVEL=2

if [ "$#" -lt 1 ]; then
    echo "Illegal number of parameters"
echo "Usage: ${0} <contiki_conf> [square|triangular|random|paper] [repnum] [sim_time in ms]"
    exit 0;
elif [ "$#" -eq 1 ]; then
	echo "Executing simulation with objective function from ${1},topology $DEFAULT_TOPOLOGY with 1 repetition";
elif [ "$#" -eq 2 ]; then
	echo "Using 1 repetition"
	echo "Executing simulation with objective function from ${1},topology ${2} with 1 repetition";
elif [ "$#" -eq 3 ]; then
	REPETITIONS=$3
	echo "Executing simulation with objective function from ${1},topology ${2} with ${3} repetitions";
elif [ "$#" -eq 4 ]; then
	REPETITIONS=$3
	SIMTIME=$4
	echo "Executing simulation with objective function from ${1},topology ${2} with ${3} repetitions, simtime: ${4}ms";
fi

#################
# Objective Function Metric
#################
# Use correct configuration file in makefile of the firmware - Parameter involved: Objective Function
start=`date +%s`


COUNTER=0
while [ $COUNTER -lt $REPETITIONS ]; do
	
	# Kaiser optimization
	if [ $COUNTER -ne 0 ] && [ $(($COUNTER % $PARALLELISM_LEVEL)) -eq 0 ]; then
		wait
	fi
	
	mkdir -p $TEMPORARY_DIRECTORY$COUNTER
	cp -r $CONTIKI_ROOT/contiki-rpl-ee $TEMPORARY_DIRECTORY$COUNTER  2> /dev/null
	
	# From now on, the base directory is in /tmp
	cd $TEMPORARY_DIRECTORY$COUNTER/contiki-rpl-ee/simulations
	rm $CONF_FILE
	
	
	case $1 in
	
	etx)
		sed 's/${METRIC}/RPL_DAG_MC_ETX/' $CONF_TEMPLATE > $CONF_FILE  
		;;
	etx_single_energy)
		sed 's/${METRIC}/RPL_DAG_MC_ETX_SINGLE_ENERGY/' $CONF_TEMPLATE > $CONF_FILE
		;;
	etx_e2e_energy)
		sed 's/${METRIC}/RPL_DAG_MC_ETX_E2E_ENERGY/' $CONF_TEMPLATE > $CONF_FILE
		;;
	e2e_energy)
		sed 's/${METRIC}/RPL_DAG_MC_ENERGY/' $CONF_TEMPLATE > $CONF_FILE
		;;
	*)
		echo "Unrecognized configuration";
		echo "Possibile configuration are: etx etx_single_energy etx_e2e_energy e2e_energy"
		exit 0;
		;;
	esac;
	
	#######################
	# Ad-Hoc Network Topology
	######################
	# Use correct cooja simulation file - Parameter involved: Topology
	
	case $2 in
	square)
	    TOPOLOGY=../examples/contiki-ee/square.csc;
	    ;;
	triangular)
	    TOPOLOGY=topologies/triangular.csc;
	    ;;
	random)
	    TOPOLOGY=../examples/contiki-ee/random-square.csc; 
	    ;;
	hexagonal)
	    TOPOLOGY=../examples/contiki-ee/hexagonal-simulation.csc;
	    ;;
	variant_hexagonal)
            TOPOLOGY=../examples/contiki-ee/variant-hexagonal.csc;
            ;;
	traffic)
	    TOPOLOGY=../examples/contiki-ee/traffic_sim_better.csc
	    ;;
	paper)
	    TOPOLOGY=$DEFAULT_TOPOLOGY;
	    ;;
	example)
            COOJA_CMD+="-nogui="$DEFAULT_TOPOLOGY" ";
	    ;;
	*)
	echo "Unrecognized topology or not specified, using default topology";
		TOPOLOGY=$DEFAULT_TOPOLOGY;
	    ;;
	esac;


	# Use Chosen topology
	COOJA_CMD+="-nogui="$TOPOLOGY" ";

	# Simulation time
	sed -i 's/${SIMTIME}/'"$SIMTIME"'/' $TOPOLOGY
	
	# Random seed
	sed -i 's/generated/'"${RANDOM_SEED[$COUNTER]}"'/' $TOPOLOGY 
	
	# Preparing result directory
	mkdir -p $RESULT_DIRECTORY
	TIMESTAMP=$(date +"%Y-%m-%d_%H-%M-%S")
	mkdir -p $RESULT_DIRECTORY/$1_$2_$TIMESTAMP
	
	
	COOJA_CMD+="-contiki=.. "
	echo "Starting simulation #" $((COUNTER+1))
	
	# After the simulation, copy the result into a specified folder
	$COOJA_CMD &> /dev/null || cp $TEMPORARY_DIRECTORY$COUNTER/contiki-rpl-ee/simulations/$COOJA_LOG $RESULT_DIRECTORY/$1_$2_$TIMESTAMP &
	let COUNTER=COUNTER+1
	sleep 2
done

echo "Waiting for all simulations to finish..."
wait
sleep 2
echo "Removing temp directory..."
rm -rf $TEMPORARY_DIRECTORY*
end=`date +%s`
runtime=$((end-start))
echo "Finished. Total runtime: " $runtime "seconds"
