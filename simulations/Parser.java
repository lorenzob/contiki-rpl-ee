import java.io.*;
import java.util.*;


/**
 * Two Output File: 1 with energy in time per each node fo single run
 * another final global output file for final energy snapshot after stability for overall runs
 * */
public class Parser {

    private static final String BASE_FOLDER = System.getProperty("user.home") + "/contiki-sim-temp";
    private static final int MOTE_NUMBER = 25;
    private static final String LOG_SEPARATOR = "\\|";


    static FileWriter finalEnergySnapshot;
    static FileWriter PDRFile;
    static FileWriter fileSentEachNode;
    static FileWriter fileReceivedEachNode;
    static FileWriter PDREachNode;
    static FileWriter graphFile;
    static FileWriter collisionFile;

    // Used for the overall experiments. Include the average of all the repetitions
    static HashMap<Integer, EnergyInfo> energyByNodes = new HashMap<>();
    static int[] packetSentByNodes = new int[MOTE_NUMBER];
    static int[] packetReceivedByNodes = new int[MOTE_NUMBER];
    static long[] sendingTimestampByNode = new long[MOTE_NUMBER];

    static HashMap<Integer, Integer> preferredParentByNode = new HashMap<>();
    static int repetition = 1;

    static class EnergyInfo {

        private static int INTERVAL_SNAPSHOT_ENERGY = 120000;
        private final boolean ENABLE_INTERMEDIATE_SNAPSHOTS = true;

        static {

            // Force a minimum interval
            if(INTERVAL_SNAPSHOT_ENERGY < 120000)
                INTERVAL_SNAPSHOT_ENERGY = 120000;
        }

        int nodeId;
        long lastSnapshot;
        long updateTimestamp;
        long cpuTime;
        long radioIdleTime;
        long radioTxTime;
        long radioRxTime;
        long sensorTime;
        FileWriter energyHistory;

        public EnergyInfo(long time, String msg, Integer nodeId) {

            this.nodeId = nodeId;

            try {

                energyHistory = new FileWriter("energy_history_node_"+ nodeId + ".csv");
                energyHistory.write("timestamp, energy_level\n");

            } catch (IOException e) {
                e.printStackTrace();
            }

            lastSnapshot = 0;
            update(time, msg);

        }

        public void update(long time, String msg){

            updateTimestamp = time;
            String[] energyChunks = msg.substring(2).split(" ");
            cpuTime = Long.parseLong(energyChunks[0]);
            radioIdleTime = Long.parseLong(energyChunks[1]);
            radioTxTime = Long.parseLong(energyChunks[2]);
            radioRxTime = Long.parseLong(energyChunks[3]);
            sensorTime = Long.parseLong(energyChunks[4]);

            if (ENABLE_INTERMEDIATE_SNAPSHOTS)
                trySnapshot();
        }

        private void trySnapshot() {
            if(updateTimestamp - lastSnapshot > INTERVAL_SNAPSHOT_ENERGY){

                try {
                    energyHistory.write(updateTimestamp + "," + getEnergy() + "\n");
                    energyHistory.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                lastSnapshot = updateTimestamp;
            }
        }


        public String getEnergy() {
            return String.valueOf((double) (radioRxTime + radioTxTime) /(cpuTime + sensorTime + radioIdleTime));
        }

        public double getEnergyPercentage() {
            return (double) (radioRxTime + radioTxTime) * 100 /(cpuTime + sensorTime + radioIdleTime);
        }
    }

    public static void main(String[] args) throws IOException {

        // Init Output File

        File graphFolder = null;


        try {

            finalEnergySnapshot = new FileWriter(BASE_FOLDER + "/final_snapshot.csv");
            PDRFile = new FileWriter(BASE_FOLDER + "/pdr.csv");
            fileSentEachNode = new FileWriter(BASE_FOLDER + "/packet_sent_eachnode.csv");
            fileReceivedEachNode = new FileWriter(BASE_FOLDER + "/packet_received_eachnode.csv");
            PDREachNode = new FileWriter(BASE_FOLDER + "/pdr_eachnode.csv");
            graphFolder = new File(BASE_FOLDER + "/graph");
            collisionFile =  new FileWriter(BASE_FOLDER + "/collision.csv");

            if (!graphFolder.exists())
                graphFolder.mkdir();

        } catch (IOException e) {
            e.printStackTrace();
        }


        if (graphFolder == null || !graphFolder.exists())
            throw new RuntimeException("cartella grafici non creata correttamente");

        File folder = new File(BASE_FOLDER);
        File[] listOfFiles = folder.listFiles();

        for(File file: listOfFiles) {

            if (file.isDirectory() && !file.getName().contains("graph")) {

                System.out.println("Parsing " + file.getName());
                graphFile = new FileWriter(BASE_FOLDER + "/" + graphFolder.getName() + "/graph" + repetition + ".gv");

                // Parse Input File
                try {
                    BufferedReader br = new BufferedReader(new FileReader(new File(file.getAbsolutePath() + "/COOJA.testlog")));

                    String randomSeed = br.readLine();
                    String line = br.readLine();

                    double packetSent = 0, packetReceived = 0, packetDuplicate = 0;
                    int collision = 0, noack = 0;
                    HashSet<String> duplicates = new HashSet<>();

                    while (line != null) {

                        String[] parts = line.split(LOG_SEPARATOR);

                        if (parts.length < 2) {
                            System.out.println(parts[0]);
                            break;
                        }

                        // Extract info from log lines
                        Long time = Long.parseLong(parts[0]);
                        String msg = parts[2];
                        Integer nodeId = Integer.parseInt(parts[1]);

                        // Chain of parsing Functions
                        if (isEnergyMessage(msg)) {

                            EnergyInfo nodeInfo = energyByNodes.get(nodeId);
                            if (nodeInfo == null) {
                                nodeInfo = new EnergyInfo(time, msg, nodeId);
                                energyByNodes.put(nodeId, nodeInfo);
                            } else
                                nodeInfo.update(time, msg);

                        } else if (isSendingMessage(msg)) {
                            packetSent++;
                            packetSentByNodes[nodeId-1]++;
                            sendingTimestampByNode[nodeId-1] = time;
                        } else if (isReceivedMessage(msg)) {

                            String[] packetChunk = msg.split(" ");
                            int originator = Integer.parseUnsignedInt(packetChunk[1]);

                            int senderNodeId;
                            // Per i vecchi log l'id del nodo era sui primi 8 bit
                            if (packetChunk[1].length() >= 3) {
                                senderNodeId =  ((originator >> 8) & 0xFF);
                            } else
                                // Nei nuovi log invece stampiamo solo i primi 8 bit direttamente
                                senderNodeId = originator;

                            int seqnum = Integer.parseInt(packetChunk[2]);
                            String key = originator + "-" + seqnum;

                            if (!duplicates.contains(key)) {
                                packetReceived++;
                                packetReceivedByNodes[senderNodeId-1]++;
                                duplicates.add(key);
                            } else
                                packetDuplicate++;
                        } else if (isNeighborInfo(msg)){

                            if(msg.contains("p (")) {
                                try {
                                    Integer preferredParent = Integer.parseInt(msg.substring(8,12).trim());
                                    preferredParentByNode.put(nodeId, preferredParent);
                                } catch(Exception e){
                                    System.out.println("Error parsing: " + msg);
                                }
                            }
                        } else if (msg.startsWith("mac: collision")) {
                            Integer tx = Integer.parseInt("" + msg.charAt(21));
                            collision += tx;
                        } else if (msg.startsWith("mac: noack")) {
                            Integer tx = Integer.parseInt("" + msg.charAt(17));
                            noack += tx;
                        }

                        line = br.readLine();

                    }

                    // Perform final step
                    try {

                        updateFinalOverallEnergySnapshot();
                        printPDRFile(packetReceived, packetSent, packetDuplicate);
                        printGraphFile();
                        printCollisionFile(collision, noack);


                        repetition++;
                        energyByNodes.clear();
                        preferredParentByNode.clear();
                        packetSentByNodes = new int[MOTE_NUMBER];
                        packetReceivedByNodes = new int[MOTE_NUMBER];


                        System.out.println("Next file .. ");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }

        finalEnergySnapshot.close();
        fileSentEachNode.close();
        PDRFile.close();

    }

    private static void printCollisionFile(int collision, int noack) throws IOException {

        if (repetition == 1) {
            collisionFile.write("Repetition, collision, noack\n");
        }

        collisionFile.write("Rep#" + repetition + ",");
        collisionFile.write(String.valueOf(collision) + "," + String.valueOf(noack) + "\n");
        collisionFile.flush();

    }

    private static void printGraphFile() {
        try {

            graphFile.append("digraph network {\n");
            preferredParentByNode.forEach((nodeId, parent) -> {
                try {
                    graphFile.append(nodeId + " -> " + parent + ";\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            graphFile.append("}");
            graphFile.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printPDRFile(double packetReceived, double packetSent, double packetDuplicate) throws IOException {

        // Write Header
        if (repetition == 1) {
            PDRFile.write("Network, PDR, duplicate\n");
            printHeaderPacketEachNodeInfo(fileReceivedEachNode);
            printHeaderPacketEachNodeInfo(fileSentEachNode);
            printHeaderPacketEachNodeInfo(PDREachNode);
        }

        PDRFile.write("Rep#" + repetition + ",");
        double pdr = packetReceived/packetSent;
        PDRFile.write(String.valueOf(pdr) + "," + packetDuplicate + "\n");
        PDRFile.flush();


        // Trick per chiamare la stessa funzione da double[] e int[]
        printPacketEachNodeInfo(fileReceivedEachNode, Arrays.stream( packetReceivedByNodes ).boxed().toArray( Integer[]::new ));
        printPacketEachNodeInfo(fileSentEachNode, Arrays.stream( packetSentByNodes ).boxed().toArray( Integer[]::new ));

        double[] pdrEachNode = new double[MOTE_NUMBER];
        for (int i = 0; i < pdrEachNode.length; i++) {
            if (packetSentByNodes[i] > 0)
                pdrEachNode[i] = packetReceivedByNodes[i] * 100 /packetSentByNodes[i];
            else
                pdrEachNode[i] = -1;    // significa nessun pacchetto inviato

        }

        printPacketEachNodeInfo(PDREachNode, Arrays.stream( pdrEachNode ).boxed().toArray( Double[]::new ));
    }

    private static void printHeaderPacketEachNodeInfo(FileWriter fileEachNode) throws IOException {
        fileEachNode.write("Repetition,");
        for (int i = 1; i <= MOTE_NUMBER; i++) {
            if (i != 1)
                fileEachNode.write(",");
            fileEachNode.write(String.valueOf(i));
        }
        fileEachNode.write("\n");
    }

    private static void printPacketEachNodeInfo(FileWriter fileEachNode, Number[] info) throws IOException {

        fileEachNode.write("Rep#" + repetition + ",");
        for (int i = 0; i < MOTE_NUMBER; i++) {
            if (i != 0)
                fileEachNode.write(",");
            fileEachNode.write(String.valueOf(info[i]));
        }
        fileEachNode.write("\n");
        fileEachNode.flush();

    }



    private static void updateFinalOverallEnergySnapshot() throws IOException {

        EnergyInfo[] snapshot = new EnergyInfo[MOTE_NUMBER];
        for (Map.Entry<Integer, EnergyInfo> entry: energyByNodes.entrySet()){

            // Print local
            entry.getValue().trySnapshot();

            // Prepare Global
            snapshot[entry.getKey()-1] = entry.getValue();
        }

        boolean first = true;
        // Write Header
        if(repetition == 1) {
            finalEnergySnapshot.write("Node,");
            for (int i = 0; i < snapshot.length; i++){
                if (!first)
                    finalEnergySnapshot.write(",");
                else
                    first = false;

                finalEnergySnapshot.write(String.valueOf(i+1));
            }
            finalEnergySnapshot.write("\n");
        }

        finalEnergySnapshot.write("Rep#" + repetition + ",");

        first = true;

        for (int i = 0; i < snapshot.length; i++) {
            EnergyInfo nodeInfo = snapshot[i];

            if (!first)
                finalEnergySnapshot.write(",");
            else
                first = false;

            if(nodeInfo == null)
                finalEnergySnapshot.write("null");
            else
                finalEnergySnapshot.write(String.valueOf(nodeInfo.getEnergyPercentage()));



        }

        finalEnergySnapshot.write("\n");
        finalEnergySnapshot.flush();

    }

    public static boolean isEnergyMessage(String msg){
        return msg.startsWith("E ");
    }

    private static boolean isSendingMessage(String msg) {
        return msg.startsWith("S ") && !msg.startsWith("S NONE");
    }

    private static boolean isReceivedMessage(String msg) {
        return msg.startsWith("R ");
    }

    private static boolean isNeighborInfo(String msg) {
        return msg.startsWith("RPL:");
    }
}
