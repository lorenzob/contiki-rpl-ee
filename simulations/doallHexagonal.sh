#!/bin/bash

echo "Starting ETX simulations"
./cooja_simulation.sh etx hexagonal 16
echo "Done"

echo "Starting E2E_ENERGY"
./cooja_simulation.sh e2e_energy hexagonal 16
echo "Done"

echo "Starting ETX_SINGLE_ENERGY" 
./cooja_simulation.sh etx_single_energy hexagonal 16
echo "Done"

echo "Starting ETX_E2E_ENERGY"
./cooja_simulation.sh etx_e2e_energy hexagonal 16
echo "Done"


